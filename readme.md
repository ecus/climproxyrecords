---
date: '22 November, 2019'
output: 
  html_document: 
    keep_md: yes
---

# climproxy records has moved to Github
https://github.com/EarthSystemDiagnostics/climproxyrecords


If you have a local Git repository you can change it to point to the new remote with these commands:

git remote -v

git remote set-url origin git@github.com:EarthSystemDiagnostics/climproxyrecords.git



